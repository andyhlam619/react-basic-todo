from django.contrib import admin
from .models import TodoItem, TodoList

# Register your models here.

@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "id",
    )

@admin.register(TodoItem)
class TodoItemAdmin(admin.ModelAdmin):
    list_display = (
        "task",
        "list",
        "id",
    )
