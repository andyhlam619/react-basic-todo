from django.db import models

# Create your models here.

class TodoList(models.Model):
    title = models.CharField(max_length=200)

    def __str__(self):
        return self.title

class TodoItem(models.Model):
    task = models.CharField(max_length=200)
    list = models.ForeignKey(
        TodoList,
        related_name="item",
        on_delete=models.CASCADE,
    )
