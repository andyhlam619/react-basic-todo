import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Nav from './Nav';
import Main from './Main';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <Main />
    </BrowserRouter>
  );
}

export default App;
